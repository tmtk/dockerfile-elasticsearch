kibana-4.0.0-BETA2:
	gzip -d -k -f kibana-4.0.0-BETA2.tar.gz; \
	tar xvf kibana-4.0.0-BETA2.tar.gz

kibana-4.0.0-BETA2.tar.gz:
	curl -OL https://download.elasticsearch.org/kibana/kibana/kibana-4.0.0-BETA2.tar.gz

## Actions
bulk: kibana-testdata
	curl -s -XPOST `boot2docker ip`:9200/_bulk --data-binary @kibana-testdata/events.json; echo

kibana:
	cd kibana-4.0.0-BETA2; \
	./bin/kibana

es_host=`boot2docker ip`
config:
	cat kibana.yml | sed s/__ES_HOST__/$(es_host)/ > kibana-4.0.0-BETA2/config/kibana.yml

## Sample data set
# Sample data is nice, but this script seems not working as my expectation :-(
./kibana-testdata/vendor/bundle: ./kibana-testdata/script/worldcitiespop.txt
	cd kibana-testdata/script; \
	bundle install --path vendor/bundle

./kibana-testdata/script/worldcitiespop.txt: ./kibana-testdata/script/worldcitiespop.txt.gz
	cd kibana-testdata/script; \
	gzip -d -k worldcitiespop.txt.gz

./kibana-testdata/script/worldcitiespop.txt.gz:
	cd kibana-testdata/script; \
	curl -OL http://download.maxmind.com/download/worldcities/worldcitiespop.txt.gz

./kibana-testdata:
	git submodule init --update
